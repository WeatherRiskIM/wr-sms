from abc import abstractmethod, ABCMeta
from base64 import standard_b64encode
from http.client import HTTPConnection
from urllib.parse import urlencode
from xml.etree import ElementTree


class RequestSubmitter(metaclass=ABCMeta):
    def __init__(self, api_host, api_port, sys_id):
        self.__api_host = api_host
        self.__api_port = api_port
        self.__sys_id = sys_id

    @staticmethod
    @abstractmethod
    def get_api_url() -> str:
        pass

    def submit_request(self, request_xml: str) -> str:
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = urlencode({'xml': request_xml})

        connection = HTTPConnection(self.__api_host, self.__api_port)
        connection.request('POST', self.get_api_url(), body, headers)
        response = connection.getresponse()
        response_byte_str = response.read()
        connection.close()

        return response_byte_str.decode('utf-8')

    def _build_basic_request_xml(self, root_name: str) -> ElementTree.Element:
        root = ElementTree.Element(root_name)

        sys_id_elem = ElementTree.SubElement(root, 'SysId')
        sys_id_elem.text = self.__sys_id

        return root

    @abstractmethod
    def build_request_xml(self) -> str:
        pass


class MessageSender(RequestSubmitter):
    def __init__(self, api_host, api_port, sys_id, src_address):
        super().__init__(api_host, api_port, sys_id)
        self.__src_address = src_address
        self.dest_addresses = []
        self.message = ''

    @staticmethod
    def get_api_url():
        return '/mpushapi/smssubmit'

    def add_dest_address(self, dest_address) -> 'MessageSender':
        self.dest_addresses.append(dest_address)

        return self

    def set_message(self, msg: str) -> 'MessageSender':
        base64_byte_str = standard_b64encode(msg.encode('utf-8'))
        self.message = base64_byte_str.decode('utf-8')

        return self

    def send_message(self) -> str:
        return self.submit_request(self.build_request_xml())

    def build_request_xml(self) -> str:
        root = self._build_basic_request_xml('SmsSubmitReq')

        src_address_elem = ElementTree.SubElement(root, 'SrcAddress')
        src_address_elem.text = self.__src_address

        for dest_address in self.dest_addresses:
            dest_address_elem = ElementTree.SubElement(root, 'DestAddress')
            dest_address_elem.text = dest_address

        message_elem = ElementTree.SubElement(root, 'SmsBody')
        message_elem.text = self.message

        return ElementTree.tostring(root)


class DRGetter(RequestSubmitter):
    def __init__(self, api_host, api_port, sys_id):
        super().__init__(api_host, api_port, sys_id)
        self.dest_addresses = []
        self.msg_id = ''

    @staticmethod
    def get_api_url():
        return '/mpushapi/smsquerydr'

    def add_dest_address(self, dest_address) -> 'DRGetter':
        self.dest_addresses.append(dest_address)

        return self

    def set_message_id(self, msg: str) -> 'DRGetter':
        self.msg_id = msg

        return self

    def query_dr(self) -> str:
        return self.submit_request(self.build_request_xml())

    def build_request_xml(self) -> str:
        root = self._build_basic_request_xml('SmsQueryDrReq')

        msg_id_elem = ElementTree.SubElement(root, 'MessageId')
        msg_id_elem.text = self.msg_id

        for dest_address in self.dest_addresses:
            dest_address_elem = ElementTree.SubElement(root, 'DestAddress')
            dest_address_elem.text = dest_address

        return ElementTree.tostring(root)
